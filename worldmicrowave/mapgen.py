import numpy as np
import warnings

from . import gridder
from . import terrain
from . import nukeparser

class RectMap(object):
    """ Basic 2D rectangular CR/Mapnuke-inspired map with wrapping """
    def __init__(self, nations, gridshape, **kwargs):
        self.gridshape = gridshape
        self.nations = nations
        self.nthrones = kwargs.get("nthrones", len(self.nations))
        self.grid = None

        #CONFIG
        #cluster water nations (cap is water) together
        self.clusterwater = True 

        #cluster water nations and coastal nations (cap is land, ring has water)
        self.clustercoastal = True 

        #reserve mountain feature for nodes with mountain-like edges
        self.restrictmountains = True 

        #chance of intentionally placing dense terrain features (farmland, 
        #large) beyond the second ring if possible
        self.distantcities = 0.5 

        #chance of intentionally placing rough terrain (wastes, swamps,
        #highlands, forests, small) in the second ring if possible
        self.nearbyobstacles = 0.5

        #hydrology
        #how often to treat swamps as equivalent to water provinces for river pathing 
        self.wetwetlands = 0.7

        #how often to start a new river instead of elongating an existing one
        self.rivernucleation = 0.3

        #how often a river forks instead of outright elongating
        self.riverbranching = 0.3

        #what fraction of river-adjacent flat provinces can be marked as
        #freshwater provinces
        #looking at dom5inspector, this only allows river flooding events
        self.floodplains = 0.3

        #minimum number of neighboring sea provinces needed to deepen a sea province
        self.deepthreshold = 3

        #allow inland river mouths as a last resort
        #particularly dry maps may end up without rivers if this option
        #is too low
        #TODO: special renderer for inland river deltas
        self.okavango = 0.9

        #orogeny
        #TODO: implement expansion sectors like Cartographic Revision
        #may be as easy as incrementing river weights

        #increment mountain weight for this fraction of highland-adjacent connections
        self.mountainspreferhighlands = 0.2

        #increment mountain weight for this fraction of forest-adjacent connections
        self.mountainspreferforests = 0.2

        #enact the Texas sharpshooter fallacy by planting mountains in
        #this fraction of waste connections
        self.createrainshadows = 0.3

        #increment mountain weight for this fraction of land-sea connections
        #TODO: special rendering for these
        #TODO: do Dom5 players pine for maps with fjords? implement a special
        #renderer for highland-river-highland
        #self.coastalmountains = 0.1

        #fraction of mountainside provinces that generate mountain-flagged
        #provinces
        self.prominentmountains = 0.3

        #how often to start a new mountain range instead of elongating an existing one
        self.mountainnucleation = 0.3

        #how often a mountain range forks instead of outright elongating
        self.mountainbranching = 0.1

        #roads
        #fraction of large provinces to boost road spawning chances for
        self.roadspreferlarge = 0.3

        #fraction of small provinces to reduce road spawning chances for
        self.roadsdislikesmall = 0.3
        
        #how often to start a new road instead of elongating an existing one
        self.roadnucleation = 0.3

        #how often to fork a road instead of outright elongating
        #FIXME: activate branch conns AFTER the "main" path is elongated
        self.roadbranching = 0.3

        #TERRAIN TYPE DISTRIBUTION
        #values stolen from MapNuke's (sane) defaults
        #order follows Pymous' population modifiers infographic
        self.minwaste = kwargs.get("minwaste", 0.04)
        self.maxwaste = kwargs.get("maxwaste", 0.06)

        self.minswamp = kwargs.get("minswamp", 0.04)
        self.maxswamp = kwargs.get("maxswamp", 0.06)

        self.minmountain = kwargs.get("minmountains", 0.05)
        self.maxmountain = kwargs.get("maxmountains", 0.07)

        self.mincaveforest = kwargs.get("mincaveforest", 0.02)
        self.maxcaveforest = kwargs.get("mincaveforest", 0.04)

        self.mincave = kwargs.get("mincave", 0.05)
        self.maxcave = kwargs.get("maxcave", 0.07)

        self.minhighland = kwargs.get("minhighland", 0.04)
        self.maxhighland = kwargs.get("maxhighland", 0.06)

        self.minforest = kwargs.get("minforest", 0.16)
        self.maxforest = kwargs.get("maxforest", 0.18)

        ####
        #"valuable" terrain types:
        ####

        self.minfarm = kwargs.get("minfarm", 0.14)
        self.maxfarm = kwargs.get("maxfarm", 0.16)

        self.minlake = kwargs.get("minlake", 0.05)
        self.maxlake = kwargs.get("maxlake", 0.09)

        assert sum([
            self.maxwaste,
            self.maxmountain,
            self.maxcaveforest,
            self.maxcave,
            self.maxhighland,
            self.maxforest,
            self.maxfarm,
            self.maxlake,
            ]) <= 1.0

        self.minsmall = kwargs.get("minsmall", 0.10)
        self.maxsmall = kwargs.get("minsmall", 0.14)

        self.minlarge = kwargs.get("minlarge", 0.28)
        self.maxlarge = kwargs.get("minlarge", 0.32)

        assert sum([
            self.maxsmall,
            self.maxlarge
            ]) <= 1.0

        #CONNECTION TYPE DISTRIBUTIONS

        self.minroads = kwargs.get("minroads", 0.04)
        self.maxroads = kwargs.get("minroads", 0.06)

        self.minbridges = kwargs.get("minbridges", 0.06)
        self.maxbridges = kwargs.get("minbridges", 0.08)

        self.minrivers = kwargs.get("minrivers", 0.04)
        self.maxrivers = kwargs.get("minrivers", 0.06)

        self.minmountains = kwargs.get("minmountains", 0.06)
        self.maxmountains = kwargs.get("minmountains", 0.08)

        self.minpasses = kwargs.get("minpasses", 0.04)
        self.maxpasses = kwargs.get("minpasses", 0.06)

        assert sum([
            self.maxroads,
            self.maxbridges,
            self.maxrivers,
            self.maxmountains,
            self.maxpasses
            ]) <= 1.0

        #LAYERS
        self.nationstarts = None
        self.terrain = None

        #MISC. VARS
        self.startpoints = None
        self.thronepoints = None

        self.predetermined = 0

    def generate(self):
        self.grid = gridder.RectGrid(self.gridshape)
        self.grid.shuffle_diagonals()

        self.initialize_layers()

        self.place_starts()
        self.fill_seas()
        self.roll_terrain_quotas()
        self.paint_terrain()

        self.run_rivers()
        self.raise_mountains()
        self.lay_roads()


    def initialize_layers(self):
        self.nationstarts = self.grid.semicopy()
        self.nationstarts.contents[:,:] = -1

        self.terrain = self.grid.semicopy()
        self.water = self.grid.semicopy()
        self.locked = self.grid.semicopy()
        self.sizelocked = self.grid.semicopy()

        self.allspecialconnections = gridder.DualGrid(self.grid.shape)
        self.specialconnectionlist = set()

        self.allrivers = gridder.DualGrid(self.grid.shape)
        self.allrivers[self.water.contents > 0] = 44
        self.allrivers.contents[(self.terrain.contents & terrain.add_terrain("swamp")) > 0] = 46
        self.allrivers.horizontals[:,:] = 0
        self.allrivers.verticals[:,:] = 0
        self.allrivers.diagonals[:,:] = 0

        self.rivers = self.allrivers.copy()
        self.bridgedrivers = self.allrivers.copy()

        self.allmountains = gridder.DualGrid(self.grid.shape)
        self.allmountains[self.water.contents > 0] = 44
        #self.allmountains.contents[(self.terrain.contents & terrain.add_terrain("swamp")) > 0] = 46
        self.allmountains.horizontals[:,:] = 0
        self.allmountains.verticals[:,:] = 0
        self.allmountains.diagonals[:,:] = 0


        self.allroads = gridder.DualGrid(self.grid.shape)
        self.allroads[self.water.contents > 0] = 44
        #self.allroads.contents[(self.terrain.contents & terrain.add_terrain("swamp")) > 0] = 46
        self.allroads.horizontals[:,:] = 0
        self.allroads.verticals[:,:] = 0
        self.allroads.diagonals[:,:] = 0

        self.roads = self.allroads.copy()
        self.mountains = self.allmountains.copy()
        self.mountainpasses = self.allmountains.copy()

    def cluster_nations(self):
        water = []
        coastal = []
        for nationid, nation in enumerate(self.nations):
            if terrain.is_sea(nation.capterrain): water.append(nationid)
            else:
                for ringterr in nation.ringterrain:
                    if terrain.is_water(ringterr):
                        coastal.append(nationid)
                        break
        #no need to cluster land starts
        if (len(water) + len(coastal)) <= 1: return
        #and no sense in clustering them if the only ones available are already water/coastal
        if (len(water) + len(coastal)) == len(self.nations): return

        distances = np.zeros((len(self.startpoints), len(self.startpoints)))

        for i1, start1 in enumerate(self.startpoints):
            for i2, start2 in enumerate(self.startpoints):
                if i1 > i2: continue
                elif i1 == i2: distances[i1,i2] = np.nan
                dist = np.sum(self.grid.get_wrap_displacement(start1, start2)**2)
                distances[i1,i2] = dist
                distances[i2,i1] = np.nan
        clustered = set()
        assignments = {}

        if len(water) >= 2:
            #cluster water things together

            i1, i2 = [indexlist[0] for indexlist in np.nonzero(distances == np.nanmin(distances))]
            assignments[i1] = water.pop()
            assignments[i2] = water.pop()
            distances[i1,i2] = np.nan

            if np.isnan(distances[i1]).all(): seed = i2
            elif np.isnan(distances[i2]).all(): seed = i1
            elif np.nanmin(distances[i1]) < np.nanmin(distances[i2]): seed = i1
            else: seed = i2

            while water:
                r, c = np.nonzero(distances == np.nanmin(distances[seed]))
                if not len(r): break
                nexti1, nexti2 = r[0], c[0]
                distances[nexti1,nexti2] = np.nan

                if np.isnan(distances[nexti1]).all(): nexti = i2
                elif np.isnan(distances[nexti2]).all(): nexti = i1
                elif seed == nexti1: nexti = nexti2
                else: nexti = nexti1 

                seed = nexti

                assignments[nexti] = water.pop()
        elif len(water): #cluster coast to water
            i1, i2 = [indexlist[0] for indexlist in np.nonzero(distances == np.nanmin(distances))]
            distances[i1,i2] = np.nan

            assignments[i1] = water.pop()
            assignments[i2] = coastal.pop()
            seed = i1
            
        else: #cluster coast to coast
            i1, i2 = [indexlist[0] for indexlist in np.nonzero(distances == np.nanmin(distances))]
            distances[i1,i2] = np.nan

            if np.isnan(distances[i1]).all(): i = i2
            elif np.isnan(distances[i2]).all(): i = i1
            elif np.nansum(distances[i1]) < np.nansum(distances[i2]): i = i1
            else: i = i2

            assignments[i] = coastal.pop()
            seed = i

        while coastal:
            r, c = np.nonzero(distances == np.nanmin(distances[seed]))
            if not len(r): break
            nexti1, nexti2 = r[0], c[0]
            if seed == nexti1: nexti = nexti2
            else: nexti = nexti1
            seed = nexti
            assignments[nexti] = coastal.pop()
            distances[nexti1,nexti2] = np.nan
        #TODO: use mean distance instead of nearest neighbor
        #(for games with lots of water nations, this can mean the difference 
        #between a snaking ocean and a nice blob of water

        #assignments := dict(startpoint->nation)
        #now rearrange self.nations so it matches the assignments

        newnations = [None for _ in range(len(self.nations))]
        unadded = list(range(len(self.nations)))

        for startid in assignments:
            nationid = assignments[startid]
            unadded.remove(nationid)
            newnations[startid] = self.nations[nationid]
        for newnationid, nation in enumerate(newnations):
            if nation is None: newnations[newnationid] = self.nations[unadded.pop(0)]

        self.nations = newnations
        
    def place_starts(self):

        placer = gridder.QuadraticYeetRayPlacer2D(self.gridshape, len(self.nations), self.nthrones)
        self.startpoints, self.thronepoints = placer.place()

        if self.clusterwater or self.clustercoastal: self.cluster_nations()

        thronetups = set([tuple(p) for p in self.thronepoints])

        for thronepoint in self.thronepoints:
            self.terrain.contents[tuple(thronepoint)] = terrain.add_terrain(self.terrain.contents[tuple(thronepoint)], "throne")

        for nationid, startpoint in enumerate(self.startpoints):
            #assign each start site to a nation
            self.nationstarts[tuple(startpoint)] = nationid
            nation = self.nations[nationid]

            #check for adjacent thrones and remove adjacencies
            for thronepoint in self.thronepoints:
                if self.grid.is_neighbor(startpoint, thronepoint): 
                    disp = self.grid.get_wrap_displacement(startpoint, thronepoint)
                    if disp[0] == disp[1]:
                        #st
                        #  \
                        #   th
                        if disp[0] > 0: 
                            self.grid.diagonals[tuple(startpoint)] = -1
                        #th
                        #  \
                        #   st
                        elif disp[0] < 0: 
                            self.grid.diagonals[tuple(thronepoint)] = -1
                        else:
                            #should be inaccessible because diag=0 is a missing edge
                            raise ValueError
                    elif disp[0] == -disp[1]:
                        #   st
                        #  /
                        #th
                        if disp[0] > 0: 
                            self.grid.diagonals[startpoint[0],thronepoint[1]] = 1
                        #   th
                        #  /
                        #st
                        elif disp[0] < 0: 
                            self.grid.diagonals[thronepoint[0],startpoint[1]] = 1
                        else: 
                            #should be inaccessible because diag=0 is a missing edge
                            raise ValueError
                    else: raise NotImplementedError("Horizontal/vertical edges shouldn't be turned off for now")
            nneighbors = len(self.grid.get_neighbors(startpoint))
            diagonals = list(self.grid.get_neighbors(startpoint, horizontal=False, vertical=False))
            unconnected = list(self.grid.get_neighbors(startpoint, horizontal=False, vertical=False, invert=True))
            ring = self.grid.get_ring(startpoint)

            assert len(diagonals) + len(unconnected) == 4
            while nneighbors > nation.capringsize:
                unlinkme = diagonals.pop(np.random.randint(len(diagonals)))
                nneighbors -= 1
                self.grid.flip_diagonal(startpoint, unlinkme)
            while nneighbors < nation.capringsize:
                #can happen in theory for excessively dense maps
                #in which case too bad for this player, but Dom5 isn't meant to be played with 4 provinces per player
                if not unconnected: break

                linkme = unconnected.pop(np.random.randint(len(unconnected)))
                if tuple(linkme) in thronetups: continue
                nneighbors += 1
                self.grid.flip_diagonal(startpoint, linkme)

            capring = nation.ringterrain[:]
            np.random.shuffle(capring)

            for provterr, point in zip(capring, self.grid.get_neighbors(startpoint)):
                self.terrain.contents[tuple(point)] = terrain.add_terrain(self.terrain.contents[tuple(point)], provterr)
                self.locked.contents[tuple(point)] = 1
            self.terrain.contents[tuple(startpoint)] = terrain.add_terrain(self.terrain.contents[tuple(startpoint)], nation.capterrain, "start")
            self.locked.contents[tuple(startpoint)] = 1
            self.sizelocked.contents[tuple(startpoint)] = 1

            self.predetermined += 1 + len(nation.ringterrain)

    def fill_seas(self):
        waterpercentage = np.mean([nation.waterpercentage for nation in self.nations])
        if waterpercentage == 0: return
        waterprovinces = waterpercentage * np.product(self.gridshape)
        if np.random.random() < (waterprovinces % 1): waterprovinces = np.ceil(waterprovinces)
        else: waterprovinces = np.floor(waterprovinces)

        self.water = self.terrain.semicopy()
        self.water.contents = np.vectorize(lambda x: terrain.is_water(x))(self.terrain.contents)
        currentwater = np.sum(self.water.contents)
        if currentwater > waterprovinces: return
        deficit = waterprovinces - currentwater

        timeout = 30
        timer = 0
        if currentwater == 0:
            rlist, clist = np.nonzero(~self.locked.contents)
            if not len(rlist): return #too bad, no water for you
            index = np.random.randint(len(rlist))
            currentwater += 1
            self.water.contents[rlist[index],clist[index]] = True
            self.terrain.contents[rlist[index],clist[index]] |= terrain._autoresolve("sea")

        while waterprovinces > currentwater:
            if timer > timeout: break
            rlist, clist = np.nonzero(self.water.contents)
            if not len(rlist): break #too bad, no water for you

            index = np.random.randint(len(rlist))
            seed = (rlist[index], clist[index])

            neighbors = self.grid.get_neighbors(seed)
            unlockedneighborids = np.vstack(np.nonzero(~(self.locked.contents[neighbors[:,0],neighbors[:,1]] | self.water.contents[neighbors[:,0],neighbors[:,1]]))).T
            if not len(unlockedneighborids): 
                timer += 1
                continue
            fillme = neighbors[np.random.randint(len(unlockedneighborids))]

            self.terrain.contents[fillme[0],fillme[1]] |= terrain._autoresolve("sea")
            self.water.contents[fillme[0],fillme[1]] = True
            timer = 0
            currentwater += 1

        #print(self.nations)
        #print(self.terrain.contents & (terrain.add_terrain("sea", "deepsea")))
        for point in np.vstack(np.nonzero(self.water.contents & ~np.bool8(self.locked.contents))).T:
            neighbors = self.grid.get_neighbors(point)
            waterneighbors = np.sum(self.water.contents[neighbors[:,0],neighbors[:,1]])
            if waterneighbors >= self.deepthreshold:
                self.terrain.contents[tuple(point)] |= terrain.add_terrain("deepsea")

        #print(self.terrain.contents & (terrain.add_terrain("sea", "deepsea")))
        #input()

        #assert waterprovinces <= currentwater
        #preview = self.water.semicopy()
        #preview.contents = self.water.contents * 2 + 42
        #print(preview.to_ansi())
        #input()

    def roll_terrain_quotas(self):
        self.quotas = {}
        area = np.product(self.gridshape)
        area -= self.predetermined
        edges = area * 3

        #TERRAIN
        self.quotas["waste"] = self.roundroll(area * np.random.uniform(self.minwaste, self.maxwaste))
        self.quotas["swamp"] = self.roundroll(area * np.random.uniform(self.minswamp, self.maxswamp))
        self.quotas["mountains"] = self.roundroll(area * np.random.uniform(self.minmountains, self.maxmountains))
        self.quotas["caveforest"] = self.roundroll(area * np.random.uniform(self.mincaveforest, self.maxcaveforest))
        self.quotas["cave"] = self.roundroll(area * np.random.uniform(self.mincave, self.maxcave))
        self.quotas["highland"] = self.roundroll(area * np.random.uniform(self.minhighland, self.maxhighland))
        self.quotas["forest"] = self.roundroll(area * np.random.uniform(self.minforest, self.maxforest))
        self.quotas["farm"] = self.roundroll(area * np.random.uniform(self.minfarm, self.maxfarm))
        self.quotas["lake"] = self.roundroll(area * np.random.uniform(self.minlake, self.maxlake))

        #SIZES
        self.quotas["small"] = self.roundroll(area * np.random.uniform(self.minsmall, self.maxsmall))
        self.quotas["large"] = self.roundroll(area * np.random.uniform(self.minlarge, self.maxlarge))

        #EDGES
        self.quotas["roads"] = self.roundroll(area * np.random.uniform(self.minroads, self.maxroads))
        self.quotas["bridges"] = self.roundroll(area * np.random.uniform(self.minbridges, self.maxbridges))
        self.quotas["rivers"] = self.roundroll(area * np.random.uniform(self.minrivers, self.maxrivers))
        self.quotas["mountains"] = self.roundroll(area * np.random.uniform(self.minmountains, self.maxmountains))
        self.quotas["passes"] = self.roundroll(area * np.random.uniform(self.minpasses, self.maxpasses))

    def roundroll(self, x):
        return int(np.floor(x)) if np.random.random() < (x % 1) else int(np.ceil(x))

    def place_terrain(self, brush, land=True, water=True, preference=0, scatter=False, tries=10, preview=None, previewcolor=None):
        unlocked = self.startdist.contents * ~np.bool8(self.locked.contents)
        if not (water or land): return
        elif not water: unlocked *= ~np.bool8(self.water.contents)

        if preference > 0:
            #unlocked[self.startdist == 0] = np.nan
            if self.distantcities and (np.random.random() < self.distantcities):
                valid = np.vstack(np.nonzero(unlocked == np.max(unlocked))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        elif preference < 0:
            unlocked[self.startdist == 0] = 999
            if self.nearbyobstacles and (np.random.random() < self.nearbyobstacles):
                valid = np.vstack(np.nonzero((unlocked != 999) & (unlocked == np.nanmin(unlocked)))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        else: valid = np.vstack(np.nonzero(unlocked > 0)).T
        if not len(valid): return

        choice = np.random.randint(len(valid))
        point = valid[choice]
        valid = valid[[x for x in range(len(valid)) if x != choice]]
        if scatter and len(valid):
            while len(valid):
                done = True
                farmnext = 0
                for neighbor in self.grid.get_neighbors(point):
                    if terrain.contains_terrain(self.terrain.contents[tuple(neighbor)], *brush):
                        farmnext += 1
                for neighbor in self.grid.get_neighbors(point):
                    if terrain.contains_terrain(self.terrain.contents[tuple(neighbor)], *brush):
                        choice = np.random.randint(len(valid))
                        point = valid[choice]
                        valid = valid[[x for x in range(len(valid)) if x != choice]]
                        #print(brush, "next to", brush, "!", len(valid))
                        done = False
                        break
                if done: break
            if not len(valid): 
                #try rescuing this terrain if no isolated spots are left
                if preference > 0:
                    #unlocked[self.startdist == 0] = np.nan
                    if self.distantcities and (np.random.random() < self.distantcities):
                        valid = np.vstack(np.nonzero(unlocked == np.max(unlocked))).T
                    else:
                        valid = np.vstack(np.nonzero(unlocked > 0)).T
                elif preference < 0:
                    unlocked[self.startdist == 0] = 999
                    if self.nearbyobstacles and (np.random.random() < self.nearbyobstacles):
                        valid = np.vstack(np.nonzero((unlocked != 999) & (unlocked == np.nanmin(unlocked)))).T
                    else:
                        valid = np.vstack(np.nonzero(unlocked > 0)).T
                else: valid = np.vstack(np.nonzero(unlocked > 0)).T
                #still no valid spots? skip

                if not len(valid): return
                choice = np.random.randint(len(valid))
                point = valid[choice]

        self.terrain.contents[tuple(point)] = terrain.add_terrain(self.terrain.contents[tuple(point)], *brush)
        self.locked.contents[tuple(point)] = 1

        if preview is not None and previewcolor is not None:
            preview.contents[tuple(point)] = previewcolor

    def place_size(self, brush, land=True, water=True, preference=0, scatter=False, tries=10, preview=None, previewcolor=None):
        unlocked = self.startdist.contents * ~np.bool8(self.sizelocked.contents)
        if not (water or land): return
        elif not water: unlocked *= ~np.bool8(self.water.contents)
        elif not land: unlocked *= np.bool8(self.water.contents)

        if preference > 0:
            #unlocked[self.startdist == 0] = np.nan
            if self.distantcities and (np.random.random() < self.distantcities):
                valid = np.vstack(np.nonzero(unlocked == np.max(unlocked))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        elif preference < 0:
            unlocked[self.startdist == 0] = 999
            if self.nearbyobstacles and (np.random.random() < self.nearbyobstacles):
                valid = np.vstack(np.nonzero((unlocked != 999) & (unlocked == np.nanmin(unlocked)))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        else: valid = np.vstack(np.nonzero(unlocked > 0)).T
        if not len(valid): return

        choice = np.random.randint(len(valid))
        point = valid[choice]
        valid = valid[[x for x in range(len(valid)) if x != choice]]
        if scatter and len(valid):
            while len(valid):
                done = True
                for neighbor in self.grid.get_neighbors(point):
                    if terrain.contains_terrain(self.terrain.contents[tuple(neighbor)], *brush):
                        choice = np.random.randint(len(valid))
                        point = valid[choice]
                        valid = valid[[x for x in range(len(valid)) if x != choice]]
                        #print(brush, "next to", brush, "!", len(valid))
                        done = False
                        break
                if done: break

        self.terrain.contents[tuple(point)] = terrain.add_terrain(self.terrain.contents[tuple(point)], *brush)
        self.sizelocked.contents[tuple(point)] = 1

        if preview is not None and previewcolor is not None:
            preview.contents[tuple(point)] = previewcolor

    def place_lakes(self, brush, preference=0, preview=None, previewcolor=None):
        unlocked = self.startdist.contents * ~np.bool8(self.locked.contents) * ~np.bool8(self.salty)

        if preference > 0:
            #unlocked[self.startdist == 0] = np.nan
            if self.distantcities and (np.random.random() < self.distantcities):
                valid = np.vstack(np.nonzero(unlocked == np.max(unlocked))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        elif preference < 0:
            unlocked[self.startdist == 0] = 999
            if self.nearbyobstacles and (np.random.random() < self.nearbyobstacles):
                valid = np.vstack(np.nonzero((unlocked != 999) & (unlocked == np.nanmin(unlocked)))).T
            else:
                valid = np.vstack(np.nonzero(unlocked > 0)).T
        else: valid = np.vstack(np.nonzero(unlocked > 0)).T
        if not len(valid): return

        point = valid[np.random.randint(len(valid))]

        self.terrain.contents[tuple(point)] = terrain.add_terrain(self.terrain.contents[tuple(point)], *brush)
        self.sizelocked.contents[tuple(point)] = 1
        self.water.contents[tuple(point)] = 1

        if preview is not None and previewcolor is not None:
            preview.contents[tuple(point)] = previewcolor
        

    def paint_terrain(self):
        self.startdist = self.grid.semicopy()
        self.startdist.contents -= 1

        preview = self.grid.semicopy()
        preview[self.water.contents > 0] = 44

        self.salty = self.water.copy()
        for r in range(self.salty.contents.shape[0]):
            for c in range(self.salty.contents.shape[1]):
                if self.water.contents[r,c]: continue
                neighbors = self.salty.get_neighbors((r,c))
                if self.water.contents[neighbors[:,0], neighbors[:,1]].any(): self.salty[r,c] = 1

        seed = self.startpoints[:]

        dist = 0
        while (self.startdist.contents == -1).any():
            if len(seed): self.startdist.contents[seed[:,0],seed[:,1]] = dist
            else: break
            newseed = []
            for p in seed:
                for n in self.grid.get_neighbors(p):
                    if not len(n): continue
                    if self.startdist.contents[tuple(n)] == -1: newseed.append(n)
            seed = np.array(newseed)
        
            dist += 1

        #good terrain first
        for _ in range(self.quotas["farm"]):
            self.place_terrain(["farm"], water=False, preference=1, scatter=True, preview=preview, previewcolor=43)

        for _ in range(self.quotas["large"]):
            self.place_size(["largeprov"], preference=1, scatter=True)

        #orthogonal obstacles next
        #waste
        for _ in range(self.quotas["waste"]):
            self.place_terrain(["waste"], water=False, preference=-1, preview=preview, previewcolor=45)
        #swamp
        for _ in range(self.quotas["swamp"]):
            self.place_terrain(["swamp"], water=False, preference=-1, preview=preview, previewcolor=46)
        #caveforest
        for _ in range(self.quotas["caveforest"]):
            self.place_terrain(["cave", "forest"], preference=-1, preview=preview, previewcolor=32)
        #cave
        for _ in range(self.quotas["cave"]):
            self.place_terrain(["cave"], preference=-1, preview=preview, previewcolor=35)
        #highland
        for _ in range(self.quotas["highland"]):
            self.place_terrain(["highland"], preference=-1, preview=preview, previewcolor=47)
        #forest
        for _ in range(self.quotas["forest"]):
            self.place_terrain(["forest"], preference=-1, preview=preview, previewcolor=42)
        
        #smallprov
        for _ in range(self.quotas["small"]):
            self.place_size(["smallprov"], preference=-1)

        #lakes
        for _ in range(self.quotas["lake"]):
            self.place_lakes(["sea"], preview=preview, previewcolor=44)

        preview[self.startpoints[:,0], self.startpoints[:,1]] = 41
        #print(preview.to_ansi())
        return
        #preview[self.startpoints[:,0], self.startpoints[:,1]] = 41
        ##preview[self.thronepoints[:,0], self.thronepoints[:,1]] = 45
        #print(self.nations)
        ##print(preview.to_ansi())
        #print(np.sum(self.locked.contents == 0) / np.product(self.gridshape))
        #input()

    def run_rivers(self):
        if not self.quotas["bridges"] + self.quotas["rivers"]: return
        mouthweights = np.zeros(self.grid.shape, dtype=np.int64)
        riverweights = mouthweights.copy()

        mouthweights[self.water.contents > 0] = 10
        mouthweights += 10 * (np.random.random(self.gridshape) < self.wetwetlands) * ((self.terrain.contents & terrain.add_terrain("swamp")) > 0)
        mouthweights += 5 * (np.random.random(self.gridshape) < self.okavango) * (self.water.contents == 0)

        #print(mouthweights)

        riverweights[self.water.contents == 0] += 10


        self.rivergen = RiverGen(self.grid, mouthweights, riverweights, self.water, nucleation=self.rivernucleation, branching=self.riverbranching, bidirectional=False, fusion=False, seen=self.specialconnectionlist)
        self.rivergen.generate(self.quotas["bridges"] + self.quotas["rivers"])
        #print(self.rivergen.weights)


        self.allrivers = gridder.DualGrid(self.grid.shape)
        self.allrivers.horizontals[:,:] = 0
        self.allrivers.verticals[:,:] = 0
        self.allrivers.diagonals[:,:] = 0

        self.rivers = self.allrivers.copy()
        self.bridgedrivers = self.allrivers.copy()

        rivertypes = [self.bridgedrivers] * self.quotas["bridges"] + [self.rivers] * self.quotas["rivers"]
        np.random.shuffle(rivertypes)
        #print(len(rivertypes))
        #print(np.sum(self.rivergen.dstgrid.horizontals + self.rivergen.dstgrid.verticals + np.abs(self.rivergen.dstgrid.diagonals)))
        #print(self.rivergen.connections)
        for point in np.vstack(np.nonzero(self.rivergen.dstgrid.horizontals)).T:
            rivertypes.pop().horizontals[tuple(point)] = 1
            self.allrivers.horizontals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.rivergen.dstgrid.verticals)).T:
            rivertypes.pop().verticals[tuple(point)] = 1
            self.allrivers.verticals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.rivergen.dstgrid.diagonals == 1)).T:
            rivertypes.pop().diagonals[tuple(point)] = 1
            self.allrivers.diagonals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.rivergen.dstgrid.diagonals == -1)).T:
            rivertypes.pop().diagonals[tuple(point)] = -1
            self.allrivers.diagonals[tuple(point)] = -1
        self.allrivers.contents[self.water.contents > 0] = 44

        print(self.allrivers.to_ansi())

    def raise_mountains(self):
        if not self.quotas["mountains"] + self.quotas["passes"]: return
        weights = np.zeros(self.grid.shape, dtype=np.int64)

        weights[self.water.contents == 0] = 10
        weights += 10 * (np.random.random(self.gridshape) < self.mountainspreferhighlands) * ((self.terrain.contents & terrain.add_terrain("highland")) > 0)
        weights += 5 * (np.random.random(self.gridshape) < self.mountainspreferforests) * ((self.terrain.contents & terrain.add_terrain("forest")) > 0)
        weights += 5 * (np.random.random(self.gridshape) < self.createrainshadows) * ((self.terrain.contents & terrain.add_terrain("waste")) > 0)


        self.mountaingen = LinearFeatureGen(self.grid, weights, nucleation=self.mountainnucleation, branching=self.mountainbranching, seen=self.specialconnectionlist)
        self.mountaingen.generate(self.quotas["mountains"] + self.quotas["passes"])
        #print(self.rivergen.weights)


        self.allmountains = gridder.DualGrid(self.grid.shape)
        self.allmountains.horizontals[:,:] = 0
        self.allmountains.verticals[:,:] = 0
        self.allmountains.diagonals[:,:] = 0

        self.mountains = self.allmountains.copy()
        self.mountainpasses = self.allmountains.copy()

        mountaintypes = [self.mountains] * self.quotas["mountains"] + [self.mountainpasses] * self.quotas["passes"]
        np.random.shuffle(mountaintypes)
        #print(len(mountaintypes))
        #print(np.sum(self.mountaingen.dstgrid.horizontals + self.mountaingen.dstgrid.verticals + np.abs(self.mountaingen.dstgrid.diagonals)))
        #print(self.mountaingen.connections)
        for point in np.vstack(np.nonzero(self.mountaingen.dstgrid.horizontals)).T:
            mountaintypes.pop().horizontals[tuple(point)] = 1
            self.allmountains.horizontals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.mountaingen.dstgrid.verticals)).T:
            mountaintypes.pop().verticals[tuple(point)] = 1
            self.allmountains.verticals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.mountaingen.dstgrid.diagonals == 1)).T:
            mountaintypes.pop().diagonals[tuple(point)] = 1
            self.allmountains.diagonals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.mountaingen.dstgrid.diagonals == -1)).T:
            mountaintypes.pop().diagonals[tuple(point)] = -1
            self.allmountains.diagonals[tuple(point)] = -1
        self.allmountains.contents[self.water.contents > 0] = 44

        print(self.mountaingen.connections)
        print(self.allmountains.to_ansi())

    def lay_roads(self):
        if not self.quotas["roads"]: return
        weights = np.zeros(self.grid.shape, dtype=np.int64)

        weights[self.water.contents == 0] = 10
        weights += 10 * (np.random.random(self.gridshape) < self.roadspreferlarge) * ((self.terrain.contents & terrain.add_terrain("largeprov")) > 0)
        weights -= 5 * (np.random.random(self.gridshape) < self.roadsdislikesmall) * ((self.terrain.contents & terrain.add_terrain("smallprov")) > 0)


        self.roadgen = LinearFeatureGen(self.grid, weights, nucleation=self.roadnucleation, branching=self.roadbranching, seen=self.specialconnectionlist)
        self.roadgen.generate(self.quotas["roads"])
        #print(self.rivergen.weights)


        self.allroads = gridder.DualGrid(self.grid.shape)
        self.allroads.horizontals[:,:] = 0
        self.allroads.verticals[:,:] = 0
        self.allroads.diagonals[:,:] = 0

        self.roads = self.allroads.copy()
        self.roadpasses = self.allroads.copy()

        roadtypes = [self.roads] * self.quotas["roads"]
        #TODO: implement dirt/stone roads
        np.random.shuffle(roadtypes)
        #print(len(roadtypes))
        #print(np.sum(self.roadgen.dstgrid.horizontals + self.roadgen.dstgrid.verticals + np.abs(self.roadgen.dstgrid.diagonals)))
        #print(self.roadgen.connections)
        for point in np.vstack(np.nonzero(self.roadgen.dstgrid.horizontals)).T:
            roadtypes.pop().horizontals[tuple(point)] = 1
            self.allroads.horizontals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.roadgen.dstgrid.verticals)).T:
            roadtypes.pop().verticals[tuple(point)] = 1
            self.allroads.verticals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.roadgen.dstgrid.diagonals == 1)).T:
            roadtypes.pop().diagonals[tuple(point)] = 1
            self.allroads.diagonals[tuple(point)] = 1
        for point in np.vstack(np.nonzero(self.roadgen.dstgrid.diagonals == -1)).T:
            roadtypes.pop().diagonals[tuple(point)] = -1
            self.allroads.diagonals[tuple(point)] = -1
        self.allroads.contents[self.water.contents > 0] = 44

        print(self.allroads.to_ansi())



class LinearFeatureGen(object):
    def __init__(self, srcgrid, weights, nucleation=0.3, branching=0.3, bidirectional=True, fusion=False, seen=None):
        self.srcgrid = srcgrid
        self.dstgrid = gridder.DualGrid(self.srcgrid.shape)
        self.dstgrid.horizontals[:,:] = 0
        self.dstgrid.verticals[:,:] = 0
        self.dstgrid.diagonals[:,:] = 0

        self.weights = weights
        self.nucleation = nucleation
        self.branching = branching
        self.bidirectional = bidirectional
        self.fusion = fusion
        self.seen = set() if seen is None else seen
        self.planned = set()

        self.elongateme = []
        self.prebranchme = {} #store branch conns here until their elongates have been activated
        self.branchme = []

        self.connections = []
    def generate(self, count):
        self.initiate()
        for _ in range(1, count):
            if np.random.random() < self.nucleation: self.initiate()
            elif np.random.random() < self.branching: self.branch()
            else: self.elongate()

    def initiate(self):
        for point1 in np.vstack(np.nonzero((self.weights == np.max(self.weights)) & (self.weights > 0))).T:
            #point1 is guaranteed to be valid
            for point2 in self.srcgrid.get_neighbors(point1):
                #point2 is not guaranteed to be valid
                conn1 = self.srcgrid.get_connection_by_points(point1, point2)

                #avoid generating features on top existing features
                if (conn1[0], tuple(conn1[1])) in self.seen: continue
                if not self.check_nucleus(point1, point2): continue
                #print("@@@", conn1)

                self.process(conn1)
                return

    def place_connection(self, conntype, point):
        self.connections.append((conntype, point))
        self.weights[tuple(point)] = 0
        if conntype == "h": self.dstgrid.horizontals[tuple(point)] = 1
        elif conntype == "v": self.dstgrid.verticals[tuple(point)] = 1
        elif conntype == "d+": self.dstgrid.diagonals[tuple(point)] = 1
        elif conntype == "d-": self.dstgrid.diagonals[tuple(point)] = -1

    def elongate(self):
        if not self.elongateme: 
            self.initiate()
            return

        while self.elongateme:
            conn1 = self.elongateme.pop(np.random.randint(len(self.elongateme)))
            x1, p1, p2, x2 = self.srcgrid.get_points_by_connection_vertices(*conn1)

            if (conn1[0], tuple(conn1[1])) in self.seen: continue
            if not self.check_elongation(p1, p2): continue
            self.process(conn1)
            return

    def branch(self):
        if not self.branchme:
            self.elongate()
            return

        while self.branchme:
            conn1 = self.branchme.pop(np.random.randint(len(self.branchme)))
            x1, p1, p2, x2 = self.srcgrid.get_points_by_connection_vertices(*conn1)

            if (conn1[0], tuple(conn1[1])) in self.seen: continue
            if not self.check_branching(p1, p2): continue
            self.process(conn1)
            return

    def check_nucleus(self, point1, point2):
        """ Customizable check for connection nuclei """
        return True

    def check_elongation(self, point1, point2):
        """ Customizable check for connection extensions """
        return True

    def check_branchme(self, point1, point2):
        """ Customizable check for connection extensions """
        return True

    def process(self, conn1):
        #precompute extend/branch targets
        x1, p1, p2, x2 = self.srcgrid.get_points_by_connection_vertices(*conn1)

        conn2 = self.srcgrid.get_connection_by_points(x1, p1)
        conn3 = self.srcgrid.get_connection_by_points(x1, p2)
        conn4 = self.srcgrid.get_connection_by_points(x2, p1)
        conn5 = self.srcgrid.get_connection_by_points(x2, p2)

        #prune forbidden connections
        if self.weights[tuple(x1)] == 0:
            conn2 = None
            conn3 = None
        if self.weights[tuple(x2)] == 0:
            conn4 = None
            conn5 = None
        if self.weights[tuple(p1)] == 0:
            conn2 = None
            conn4 = None
        if self.weights[tuple(p2)] == 0:
            conn3 = None
            conn5 = None

        #prune redundant connections
        if conn2 is not None and (conn2[0], tuple(conn2[1])) in self.seen:
            conn2 = None
        if conn3 is not None and (conn3[0], tuple(conn3[1])) in self.seen:
            conn3 = None
        if conn4 is not None and (conn4[0], tuple(conn4[1])) in self.seen:
            conn4 = None
        if conn5 is not None and (conn5[0], tuple(conn5[1])) in self.seen:
            conn5 = None

        #remove fusing connections if necessary
        if not self.fusion:
            if conn2 is not None and (conn2[0], tuple(conn2[1])) in self.planned:
                conn2 = None
            if conn3 is not None and (conn3[0], tuple(conn3[1])) in self.planned:
                conn3 = None
            if conn4 is not None and (conn4[0], tuple(conn4[1])) in self.planned:
                conn4 = None
            if conn5 is not None and (conn5[0], tuple(conn5[1])) in self.planned:
                conn5 = None

        #register connections so they don't get duplicated
        for conn in (conn1, conn2, conn3, conn4, conn5):
            if conn is not None:
                self.seen.add((conn[0], tuple(conn[1])))
                self.planned.add((conn[0], tuple(conn[1])))

        #place the connection where appropriate
        self.place_connection(*conn1)

        #add upcoming connections to the records
        if conn2 is None and conn4 is None:
            pass
        elif conn4 is None:
            self.elongateme.append(conn2)
            if conn3 is not None:
                self.prebranchme[conn2[0],tuple(conn2[1])] = conn3
        elif conn2 is None:
            self.elongateme.append(conn4)
            if conn5 is not None:
                self.prebranchme[conn4[0],tuple(conn4[1])] = conn5
        else:
            if self.bidirectional:
                if np.random.random() < 0.5:
                    self.elongateme.append(conn2)
                    if conn3 is not None:
                        self.prebranchme[conn2[0],tuple(conn2[1])] = conn3
                    self.elongateme.append(conn4)
                    if conn5 is not None:
                        self.prebranchme[conn4[0],tuple(conn4[1])] = conn5
                else:
                    self.elongateme.append(conn4)
                    if conn5 is not None:
                        self.prebranchme[conn4[0],tuple(conn4[1])] = conn5
                    self.elongateme.append(conn2)
                    if conn3 is not None:
                        self.prebranchme[conn2[0],tuple(conn2[1])] = conn3
            else:
                if np.random.random() < 0.5:
                    self.elongateme.append(conn2)
                    if conn3 is not None:
                        self.prebranchme[conn2[0],tuple(conn2[1])] = conn3
                else:
                    self.elongateme.append(conn4)
                    if conn5 is not None:
                        self.prebranchme[conn4[0],tuple(conn4[1])] = conn5


class BiphasicLinearFeatureGen(LinearFeatureGen):
    """ Like LinearFeatureGen, but separates initiation and elongation weights """
    def __init__(self, srcgrid, initweights, elongweights, nucleation=0.3, branching=0.3, bidirectional=True, fusion=False, seen=None):
        super().__init__(srcgrid, initweights, nucleation=nucleation, branching=branching, bidirectional=bidirectional, fusion=fusion, seen=seen)
        self.initweights = initweights
        self.elongweights = elongweights

    def elongate(self):
        if not self.elongateme:
            self.initiate()
            return
        self.weights = self.elongweights
        super().elongate()
        self.weights = self.initweights

    def branch(self):
        if not self.branchme:
            self.elongate()
            return
        self.weights = self.elongweights
        super().branch()
        self.weights = self.initweights

class RiverGen(BiphasicLinearFeatureGen):
    """ Like BiphasicLinearFeatureGen, but avoids forming water-water connections """
    def __init__(self, srcgrid, initweights, elongweights, water, nucleation=0.3, branching=0.3, bidirectional=True, fusion=False, seen=None):
        super().__init__(srcgrid, initweights, elongweights, nucleation=nucleation, branching=branching, bidirectional=bidirectional, fusion=fusion, seen=seen)
        self.water = water

    def check_nucleus(self, point1, point2):
        #specifically looking for a two land provinces neighboring a water province
        #L-L
        # \|
        #  W: need x1 | x2 to be land and p1 ^ p2 to be land
        point3, _, _, point4 = self.srcgrid.get_diamond(point1, point2)
        return ((self.water.contents[tuple(point3)] == 0) | (self.water.contents[tuple(point4)])) & ((self.water.contents[tuple(point1)] > 0) ^ (self.water.contents[tuple(point2)] > 0))

        #return (self.initweights[tuple(point1)] > 0) ^ (self.initweights[tuple(point2)] > 0)
        #return (self.water.contents[tuple(point1)] == 0) & (self.water.contents[tuple(point2)] == 0)

    def check_elongation(self, point1, point2):
        return (self.water.contents[tuple(point1)] == 0) & (self.water.contents[tuple(point2)] == 0)

    def check_branchme(self, point1, point2):
        return (self.water.contents[tuple(point1)] == 0) & (self.water.contents[tuple(point2)] == 0)
