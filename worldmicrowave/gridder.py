import numpy as np
import warnings

class FlatGrid(object):
    def __init__(self, shape):
        self.shape = shape
        self.contents = np.zeros(shape, dtype=np.int16)

class RectGrid(object):
    def __init__(self, shape):
        self.shape = shape
        self.contents = np.zeros(shape, dtype=np.int64)

        self.horizontals = np.ones(shape, dtype=np.bool8)
        self.verticals = np.ones(shape, dtype=np.bool8)
        self.diagonals = np.ones(shape, dtype=np.int8)

        self.vertices = np.zeros(shape, dtype=np.int8)

    def copy(self):
        out = type(self)(self.shape)
        out.contents = self.contents.copy()
        out.horizontals = self.horizontals.copy()
        out.verticals = self.verticals.copy()
        out.diagonals = self.diagonals.copy()
        return out

    def semicopy(self):
        """ Create a new RectGrid with linked identical edges and empty nodes 
        Useful for new data layers requiring identical connectivity """
        out = type(self)(self.shape)
        out.horizontals = self.horizontals
        out.verticals = self.verticals
        out.diagonals = self.diagonals
        return out


    def to_ascii(self):
        out = ""
        diagonals = {-1: "/", 0: " ", 1: "\\"}
        if len(self.shape) == 3:
            for layer in range(self.shape[2]):
                out += self[:,:,layer].to_ascii()
                out += "\n"
        elif len(self.shape) == 2:
            for row in range(self.shape[0]):
                for col in range(self.shape[1]):
                    out += "o" + ("-" if self.horizontals[row,col] else " ")
                out += "\n"
                for col in range(self.shape[1]):
                    out += ("|" if self.verticals[row,col] else " ") + diagonals[self.diagonals[row,col]]
                out += "\n"
        return out
    def to_ansi(self):
        out = ""
        diagonals = {-1: "/", 0: " ", 1: "\\"}
        if len(self.shape) == 3:
            for layer in range(self.shape[2]):
                out += self[:,:,layer].to_ascii()
                out += "\n"
        elif len(self.shape) == 2:
            for row in range(self.shape[0]):
                for col in range(self.shape[1]):
                    out += "\033[{}mo\033[0m".format(self.contents[row,col]) + ("-" if self.horizontals[row,col] else " ")
                out += "\n"
                for col in range(self.shape[1]):
                    out += ("|" if self.verticals[row,col] else " ") + diagonals[self.diagonals[row,col]]
                out += "\n"
        return out

    def shuffle_diagonals(self):
        self.diagonals[np.random.random(self.shape) < 0.5] = -1

    def __getitem__(self, index):
        contents = self.contents[index]
        subgrid = RectGrid(contents.shape)
        subgrid.contents = contents
        subgrid.horizontals = self.horizontals[index]
        subgrid.verticals = self.verticals[index]
        subgrid.diagonals = self.diagonals[index]
        return subgrid

    def __setitem__(self, index, value):
        self.contents.__setitem__(index, value)

    def get_neighbors(self, index, horizontal=True, vertical=True, diagonal=True, invert=False):
        index = np.array(index)
        neighbors = []
        x = np.int64([0, 1])
        y = np.int64([1, 0])
        if horizontal:
            if invert ^ self.horizontals[tuple(index)]: neighbors.append(np.mod(index + x, self.shape))
            if invert ^ self.horizontals[tuple(index-1)]: neighbors.append(np.mod(index - x, self.shape)) 

        if vertical:
            if invert ^ self.verticals[tuple(index)]: neighbors.append(np.mod(index + y, self.shape))
            if invert ^ self.verticals[tuple(index-1)]: neighbors.append(np.mod(index - y, self.shape)) 

        if diagonal:
            if invert ^ (self.diagonals[tuple(index)] > 0): neighbors.append(np.mod(index + x + y, self.shape))
            if invert ^ (self.diagonals[tuple(index-1)] > 0): neighbors.append(np.mod(index - x - y, self.shape))

            if invert ^ (self.diagonals[tuple(index-y)] < 0): neighbors.append(np.mod(index + x - y, self.shape))
            if invert ^ (self.diagonals[tuple(index-x)] < 0): neighbors.append(np.mod(index - x + y, self.shape))

        return np.int64(neighbors)

    def get_ring(self, index, horizontal=True, vertical=True, diagonal=True):
        """ Returns a list containing the coordinates of all 8 cells around a point """
        index = np.array(index)
        neighbors = []
        x = np.int64([0, 1])
        y = np.int64([1, 0])

        #TODO: roll this up into a nice for loop
        if horizontal:
            neighbors.append(np.mod(index + x, self.shape))
            neighbors.append(np.mod(index - x, self.shape)) 

        if vertical:
            neighbors.append(np.mod(index + y, self.shape))
            neighbors.append(np.mod(index - y, self.shape)) 

        if diagonal:
            neighbors.append(np.mod(index + x + y, self.shape))
            neighbors.append(np.mod(index - x - y, self.shape))

            neighbors.append(np.mod(index + x - y, self.shape))
            neighbors.append(np.mod(index - x + y, self.shape))
        return np.int64(neighbors)


    def get_wrap_displacement(self, point1, point2):
        p1 = point1
        p2 = point2
        #vertical
        #p1~p2
        if p1[0] == p2[0]: v = 0
        elif p2[0] > p1[0]:
            #  direct               wrapped
            if abs(p2[0] - p1[0]) < abs(p2[0] - self.shape[0] - p1[0]):
                v = p2[0] - p1[0]
            else:
                v = p2[0] - self.shape[0] - p1[0]
        else: #p1[0] < p2[0]
            #  direct               wrapped
            if abs(p2[0] - p1[0]) < abs(p2[0] + self.shape[0] - p1[0]):
                v = p2[0] - p1[0]
            else:
                v = p2[0] + self.shape[0] - p1[0]

        if p1[1] == p2[1]: h = 0
        elif p2[1] > p1[1]:
            #  direct               wrapped
            if abs(p2[1] - p1[1]) < abs(p2[1] - self.shape[1] - p1[1]):
                h = p2[1] - p1[1]
            else:
                h = p2[1] - self.shape[1] - p1[1]
        else: #p1[1] < p2[1]
            #  direct               wrapped
            if abs(p2[1] - p1[1]) < abs(p2[1] + self.shape[1] - p1[1]):
                h = p2[1] - p1[1]
            else:
                h = p2[1] + self.shape[1] - p1[1]

        return np.array([v, h])

    def flip_diagonal(self, point1, point2):
        disp = self.get_wrap_displacement(point1, point2)
        if (disp == 0).any(): raise ValueError("Points {} and {} are not diagonal (disp={})".format(point1, point2, disp))
        elif (np.max(np.abs(disp)) >= 2): raise ValueError("Points {} and {} are not diagonal (disp={})".format(point1, point2, disp))

        if disp[0] == disp[1]:
            #p1
            #  \
            #   p2
            if disp[0] > 0:
                self.diagonals[tuple(point1)] = -1
            #p2
            #  \
            #   p1
            elif disp[0] < 0:
                self.diagonals[tuple(point2)] = -1
            else: 
                #should never happen because this would mean a diagonal was disabled
                raise ValueError

        elif disp[0] == -disp[1]:
            #    p1
            #  /
            #p2
            if disp[0] > 0:
                self.diagonals[point1[0],point2[1]] = 1
            #    p2
            #  /
            #p1
            elif disp[0] < 0:
                self.diagonals[point2[0],point1[1]] = 1
            else:
                #should never happen because this would mean a diagonal was disabled
                raise ValueError

        else:
            #should never happen because the above should have handled everything
            raise ValueError

    def is_neighbor(self, point1, point2):
        point1 = np.array(point1)
        point2 = np.array(point2)
        displacement = self.get_wrap_displacement(point1, point2)
        if np.max(np.abs(displacement)) >= 2: return False
        if (point1 == point2).all(): return False
        #vertical
        if point1[1] == point2[1]:
            #p1
            #|
            #p2
            if displacement[0] > 0 and self.verticals[tuple(point1)]: return True
            #p2
            #|
            #p1
            elif displacement[0] < 0 and self.verticals[tuple(point2)]: return True
        #horizontal
        if point1[0] == point2[0]:
            #p1 - #p2
            if displacement[1] > 0 and self.horizontals[tuple(point1)]: return True
            #p2 - #p1
            elif displacement[1] < 0 and self.horizontals[tuple(point2)]: return True
        #diagonal
        #positive slope
        if displacement[0] == displacement[1]:
            #p1
            #  \
            #   p2
            if displacement[0] > 0 and self.diagonals[tuple(point1)] == 1: return True
            #p2
            #  \
            #   p1
            elif displacement[0] < 0 and self.diagonals[tuple(point2)] == 1: return True
        #negative slope
        elif displacement[0] != displacement[1]:
            #   p1
            #  /
            #p2
            if displacement[0] > 0 and self.diagonals[point1[0],point2[1]] == -1: return True
            #   p2
            #  /
            #p1
            elif displacement[0] < 0 and self.diagonals[point2[0],point1[1]] == -1: return True
        return False

    def is_vertical(self, point1, point2):
        disp = self.get_wrap_displacement(point1, point2)
        if disp[0] and not disp[1]: return True
        return False

    def is_horizontal(self, point1, point2):
        disp = self.get_wrap_displacement(point1, point2)
        if disp[1] and not disp[0]: return True
        return False

    def is_diagonal(self, point1, point2):
        disp = self.get_wrap_displacement(point1, point2)
        if abs(disp[0]) == abs(disp[1]): return True
        return False

    def get_vertex_by_points(self, point1, point2, point3):
        """ Return the vertex defined by three points (or None) """
        pass

    def get_points_by_connection_vertices(self, conntype, index):
        """ Return three points around each connection endpoint """
        index = np.array(index)
        x = np.array([0, 1])
        y = np.array([1, 0])

        #   E-F   E-F
        #   |\|   |/|
        # G-A-B G-A-B
        # |\|\| |/|\|
        # H-C-D H-C-D
        A = index
        B = np.int64(np.mod(index + x, self.shape))
        C = np.int64(np.mod(index + y, self.shape))
        D = np.int64(np.mod(index + y + x, self.shape))
        
        E = np.int64(np.mod(index - y, self.shape))
        F = np.int64(np.mod(index - y + x, self.shape))

        G = np.int64(np.mod(index - x, self.shape))
        H = np.int64(np.mod(index + y - x, self.shape))
        if conntype == "d":
            if self.diagonals[tuple(index)] > 0:
                common = [A, D]
                ends = [B, C]
            elif self.diagonals[tuple(index)] < 0:
                common = [B, C]
                ends = [A, D]
            else: raise ValueError("No diagonal at {}".format(index))
        elif conntype == "d+":
            common = [A, D]
            ends = [B, C]
        elif conntype == "d-":
            common = [B, C]
            ends = [A, D]
        else:
            ends = []
            if conntype == "h":
                common = [A, B]
                if self.diagonals[tuple(E)] > 0:
                    ends.append(E)
                elif self.diagonals[tuple(E)] < 0:
                    ends.append(F)
                else: raise ValueError("No diagonal at {}".format(E))
                if self.diagonals[tuple(index)] > 0:
                    ends.append(D)
                elif self.diagonals[tuple(index)] < 0:
                    ends.append(C)
                else: raise ValueError("No diagonal at {}".format(index))
            elif conntype == "v":
                common = [A, C]
                if self.diagonals[tuple(G)] > 0:
                    ends.append(G)
                elif self.diagonals[tuple(G)] < 0:
                    ends.append(H)
                else: raise ValueError("No diagonal at {}".format(G))
                if self.diagonals[tuple(index)] > 0:
                    ends.append(D)
                elif self.diagonals[tuple(index)] < 0:
                    ends.append(B)
                else: raise ValueError("No diagonal at {}".format(index))

            else: raise ValueError("Unrecognized conntype: {}".format(conntype))

        return [ends[0], common[0], common[1], ends[1]]

    def get_connection_by_points(self, point1, point2):
        if not self.is_neighbor(point1, point2): 
            raise ValueError("{} and {} are not connected".format(point1, point2))
        #raise exception or return none?

        disp = self.get_wrap_displacement(point1, point2)
        #A-B
        #|\|
        #C-D
        if disp[0] > 0: #pt1 then pt2
            start = point1
            end = point2
        elif disp[0] < 0: #pt2 then pt1
            start = point2
            end = point1
        elif disp[1] > 0:
            start = point1
            end = point2
        else: 
            start = point2
            end = point1

        start = np.array(start)

        if disp[0] == 0:
            return "h", start
        elif disp[1] == 0:
            return "v", start
        elif disp[0] == disp[1]:
            return "d+", start
        else:
            return "d-", np.array([start[0],end[1]])

    def get_diamond(self, point1, point2):
        return self.get_points_by_connection_vertices(*self.get_connection_by_points(point1, point2))

class DualGrid(RectGrid):
    def to_ascii(self):
        out = ""
        diagonals = {-1: "\\", 0: " ", 1: "/"}
        if len(self.shape) == 3:
            for layer in range(self.shape[2]):
                out += self[:,:,layer].to_ascii()
                out += "\n"
        elif len(self.shape) == 2:
            for row in range(self.shape[0]):
                for col in range(self.shape[1]):
                    out += "o" + ("|" if self.horizontals[row,col] else " ")
                out += "\n"
                for col in range(self.shape[1]):
                    out += ("-" if self.verticals[row,col] else " ") + diagonals[self.diagonals[row,col]]
                out += "\n"
        return out
    def to_ansi(self):
        out = ""
        diagonals = {-1: "\\", 0: " ", 1: "/"}
        if len(self.shape) == 3:
            for layer in range(self.shape[2]):
                out += self[:,:,layer].to_ascii()
                out += "\n"
        elif len(self.shape) == 2:
            for row in range(self.shape[0]):
                for col in range(self.shape[1]):
                    out += "\033[{}mo\033[0m".format(self.contents[row,col]) + ("|" if self.horizontals[row,col] else " ")
                out += "\n"
                for col in range(self.shape[1]):
                    out += ("-" if self.verticals[row,col] else " ") + diagonals[self.diagonals[row,col]]
                out += "\n"
        return out

class MonteCarloPlacer(object):
    def __init__(self, shape, players, thrones, trials=100):
        pass

class PoissonDiskPlacer2D(object):
    def __init__(self, shape, players, thrones):
        self.shape = shape
        self.players = players
        self.thrones = thrones
        space = np.product(shape)
        #area to allocate for each player/throne
        indivspace = space / (players + thrones)
        #target mean radius
        radius = (indivspace / np.pi) **.5
        #print(shape, players, thrones, radius)
        #actual radius used for poisson disks
        #poissonradius = radius / (2.5**.5) 
        poissonradius = radius  / 1.6 #FIXME: WHY IS THIS THE BEST CONSTANT SO FAR???
        self.poissonradius = poissonradius
        self.poissonradius2 = self.poissonradius ** 2
        #if self.poissonradius2 < 2: print("Grid is too small to avoid neighbors")
        #elif self.poissonradius2 < 9: print("Grid is too small for proper cap rings")
        #print(shape, players, thrones, self.poissonradius, self.poissonradius2)

    def place(self, tries=30):
        points = []
        points.append(np.array([np.random.randint(self.shape[0]), np.random.randint(self.shape[1])]))

        possipoints = []
        possipoints.append(points[-1])

        targetcount = self.players + self.thrones
        #while (len(points) < targetcount) and (len(possipoints)):
        while (len(points) < targetcount) and (len(possipoints)):
        #while (len(possipoints)):
            index = np.random.randint(len(possipoints))
            oldpoint = possipoints[index]
            placed = False
            for _ in range(tries):
                radius = self.poissonradius * (np.random.random() +1)
                angle = np.random.random() * 2 * np.pi
                newpoint = np.mod(oldpoint + np.int64(np.round([radius * np.sin(angle), radius * np.cos(angle)])), self.shape)
                ok = True
                for p in points: 
                    dr = min(abs(newpoint[0] - p[0]), abs(-abs(newpoint[0] - p[0]) + self.shape[0]))
                    dc = min(abs(newpoint[1] - p[1]), abs(-abs(newpoint[1] - p[1]) + self.shape[1]))
                    if (dr**2 + dc**2) < self.poissonradius2:
                        ok = False
                        break
                    #print(p, newpoint, dr**2 + dc**2, (dr, dc))
                if ok: 
                    points.append(np.int64(newpoint))
                    placed = True
                    break
                else: continue
            if not placed: possipoints.pop(index)

        return points

class ForceFieldPlacer2D(object):
    def __init__(self, shape, players, thrones, charge=1.0, drag=0.1):
        self.shape = shape
        self.players = players
        self.thrones = thrones

        self.charge = charge
        self.drag = drag

    def place(self, dt=0.1, steps=100):
        rows, cols = np.nonzero(np.ones(self.shape))
        indices = np.arange(np.product(self.shape), dtype=np.int64) 
        np.random.shuffle(indices)

        seen = {}
        x = [[rows[i], cols[i]] for i in indices[:int(self.players + self.thrones)]]
        hx = self.hash_state(x)

        if hx in seen: seen[hx] += 1
        else: seen[hx] = 1

        v0 = [np.zeros(2) for _ in len(x)]
        for _ in range(steps):
            a = []
            v = []
            for i1, x1 in enumerate(x):
                a.append(np.zeros(2))
                v.append(np.zeros(2))
                for i2, x2 in enumerate(x):
                    if i1 == i2: continue
                    displacement = wrapdisp(self.shape, x2, x1)
                    magnitude = np.sum(displacement**2)
                    a[-1] += self.charge * displacement / magnitude

                    #not a = kq1q2 * r / |r|**3
                    #but a = kq1q2 * r / |r|**2
                a[-1] -= self.drag * self.v0[i1] 
                v[-1] = v0[i1] + a[-1] * dt
            x = [np.mod(x[i] + v[i] * dt + 0.5 * a[i] * dt**2, np.shape)]
            v0 = v


        return x

    def hash_state(self, x):
        return hash(tuple([tuple(pos) for pos in x]))


class QuadraticYeetRayPlacer2D(object):
    def __init__(self, shape, players, thrones, intensity=10):
        self.shape = shape
        self.players = players
        self.thrones = thrones
        self.intensity = intensity

    def place(self):

        #prepare the kernel
        kernel = np.float64(np.indices((2 * self.intensity + 1, 2 * self.intensity + 1)))
        kernel -= self.intensity
        kernel = (kernel[0,:,:]**2 + kernel[1,:,:]**2 + 1e-9) ** -1
        kernel[kernel > 1000] = np.inf
        kernel[self.intensity,self.intensity] = self.intensity**2
        startkernel = kernel.copy()
        startkernel[self.intensity-1:self.intensity+2,self.intensity-1:self.intensity+2] = self.intensity
        startkernel[self.intensity-1:self.intensity+2,self.intensity] = self.intensity**2
        startkernel[self.intensity,self.intensity-1:self.intensity+2] = self.intensity**2

        kernel = np.roll(np.roll(np.fft.fftshift(kernel), 1, axis=0), 1, axis=1)
        startkernel = np.roll(np.roll(np.fft.fftshift(startkernel), 1, axis=0), 1, axis=1)

        shine = np.zeros(self.shape)

        #blit the players
        startpoints = []
        for _ in range(self.players):
            possir, possic = np.nonzero(shine == np.min(shine))
            index = np.random.randint(len(possir))
            startpoints.append(np.array([possir[index], possic[index]]))

            seen = set()

            for dr in range(-self.intensity, self.intensity+1):
                if abs(dr) > (self.shape[0]//2): continue
                targetr = int(np.mod(startpoints[-1][0] + dr, self.shape[0]))
                for dc in range(-self.intensity, self.intensity+1):
                    if abs(dc) > (self.shape[1]//2): continue
                    targetc = int(np.mod(startpoints[-1][1] + dc, self.shape[1]))
                    if (targetr,targetc) in seen: continue
                    seen.add((targetr, targetc))

                    shine[targetr,targetc] += startkernel[dr,dc]
        thronepoints = []
        for _ in range(self.thrones):
            possir, possic = np.nonzero((shine < self.intensity**2) & (shine == np.min(shine)))
            if not len(possir): break
            index = np.random.randint(len(possir))
            thronepoints.append(np.array([possir[index], possic[index]]))

            seen = set()

            for dr in range(-self.intensity, self.intensity+1):
                if abs(dr) > (self.shape[0]//2): continue
                targetr = int(np.mod(thronepoints[-1][0] + dr, self.shape[0]))
                for dc in range(-self.intensity, self.intensity+1):
                    if abs(dc) > (self.shape[1]//2): continue
                    targetc = int(np.mod(thronepoints[-1][1] + dc, self.shape[1]))
                    if (targetr,targetc) in seen: continue
                    seen.add((targetr, targetc))

                    shine[targetr,targetc] += kernel[dr,dc]
        return np.array(startpoints), np.array(thronepoints)

def wrapdisp(shape, p1, p2):
    #p2 - p1
    diff = []
    if -shape[0]//2 <= (p2[0] - p1[0]) <= shape[0]//2: diff.append(p2[0] - p1[0])
    #else: diff.append(-(np.sign(p2[0] - p1[0]) * shape[0] - (p2[0] - p1[0])))
    else: diff.append(-np.sign(p2[0] - p1[0]) * abs(shape[0] - abs(p2[0] - p1[0])))
    if -shape[1]//2 <= (p2[1] - p1[1]) <= shape[1]//2: diff.append(p2[1] - p1[1])
    #else: diff.append(-(np.sign(p2[1] - p1[1]) * shape[1] - (p2[1] - p1[1])))
    else: diff.append(-np.sign(p2[1] - p1[1]) * abs(shape[1] - abs(p2[1] - p1[1])))

    return np.array(diff)

class GridShaper(object):
    def __init__(self, nprovinces, mapshape, aspect=1.618, ndims=2,):
        pass

def grid_shaper(nprovinces, mapshape, aspect=1.618, ndims=2, approxdirection=1):
    try: iter(aspect)
    except TypeError: aspect = (aspect,)
    #aspect = (1/x for x in aspect)
    provspace = np.product(mapshape) / nprovinces
    #2D case:
    #dims: 
    #h = s
    #w = ks
    #A = k*s * s
    #s = sqrt(A)/k
    #3D case:
    #V = whd
    #h = s
    #w = k*s
    #d = l*s
    #V = kls**3
    #s = cbrt(V)/k/l
    #I need a better coordinate system, but oh well
    provdims = np.zeros(ndims)
    provdims[0] = provspace ** (1/ndims) / np.product(aspect)
    for i, a in enumerate(aspect):
        provdims[i+1] = provdims[0] * a
    provdims *= np.product([a**(1/ndims) for a in aspect])
    repeats = [m/p for m, p in zip(mapshape, provdims)]
    return provdims, repeats

def pick_gridshape(nprovinces, repeat, prefer=1):
    #prefer>0: pick closest count above nprovinces
    #prefer<0: pick closest count below nprovinces
    #prefer=0: pick closest count to provinces
    dims = np.zeros((2**len(repeat), 2), dtype=np.uint64)
    for i, x in enumerate(repeat):
        for t in range(2**len(repeat)):
            dims[t,i] += (t >> i) % 2
    for i, x in enumerate(repeat):
        dims[:,i] += max(1, int(np.floor(repeat[i])))
    space = np.product(dims, axis=1)

    if prefer > 0: distances = [(abs(n - nprovinces), i) for i, n in enumerate(space) if n >= nprovinces]
    elif prefer < 0: distances = [(abs(n - nprovinces), i) for i, n in enumerate(space) if n <= nprovinces]
    else: distances = [(abs(n - nprovinces), i) for i, n in enumerate(space)]

    if not distances:
        distances = [(abs(n - nprovinces), i) for i, n in enumerate(space)]
    return dims[sorted(distances)[0][1],:]
