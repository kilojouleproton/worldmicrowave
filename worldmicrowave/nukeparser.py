from . import terrain
import xml.etree.ElementTree as ET

class Nation(object):
    id = None
    name = None
    age = None
    waterpercentage = None
    capringsize = None
    capterrain = None
    ringterrain = None

    def __init__(self):
        pass

    def __repr__(self):
        return "Nation(name='{}')".format(self.name)

class NationCollection(object):
    def __init__(self, name=None):
        self.name = name
        self._nationlist = []

    def parse(self, fp):
        with open(fp) as f: tree = ET.parse(f)

        root = tree.getroot()

        for nationelem in root: 
            nation = Nation()
            ringterrain = []

            for attrib in nationelem:
                if attrib.tag == "Name": nation.name = attrib.text
                elif attrib.tag == "ID": nation.id = int(attrib.text)
                elif attrib.tag == "WaterPercentage": nation.waterpercentage = float(attrib.text)
                elif attrib.tag == "Age": nation.age = attrib.text
                elif attrib.tag == "CapRingSize": nation.capringsize = int(attrib.text)
                elif attrib.tag == "CapTerrain": nation.capterrain = terrain.add_terrain(*attrib.text.lower().split())
                elif attrib.tag == "TerrainData": ringterrain.append(terrain.add_terrain(*attrib.text.lower().split()))
            nation.ringterrain = ringterrain
            self._nationlist.append(nation)

    def get_nation_by_id(self, id):
        for nation in self._nationlist:
            if nation.id == id: return nation
        raise IndexError

    def get_nation_by_name(self, name):
        for nation in self._nationlist:
            if nation.name == name: return nation
        raise KeyError

    def __len__(self): return len(self._nationlist)

    def __getitem__(self, index): return self._nationlist.__getitem__(index)

