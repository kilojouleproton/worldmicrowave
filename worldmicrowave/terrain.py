
_terrain = {
        0:"smallprov",
        1:"largeprov",
        2:"sea",
        3:"freshwater",
        4:"highland",
        5:"swamp",
        6:"waste",
        7:"forest",
        8:"farm",
        9:"nostart",
        10:"magic",
        11:"deepsea",
        12:"cave",

        13:"fire",
        14:"air",
        15:"water",
        16:"earth",
        17:"astral",
        18:"death",
        19:"nature",
        20:"blood",
        21:"holy",
        
        22:"mountains",
        24:"throne",
        25:"start",
        26:"nothrone",
        29:"warmer",
        30:"colder",
        }

_terrain_r = dict(zip(_terrain.values(), _terrain.keys()))

def _autoresolve(code):
    if code == "plains": return 0
    elif type(code) is str: return 2**_terrain_r[code]
    else: return code

def is_plains(code): return bool(_autoresolve(code) == 0)

def is_small(code): return bool(_autoresolve(code) & (2**0))
def is_large(code): return bool(_autoresolve(code) & (2**1))
def is_sea(code): return bool(_autoresolve(code) & (2**2))
def is_water(code): return is_sea(code)
def is_land(code): return not is_sea(code)
def is_freshwater(code): return bool(_autoresolve(code) & (2**3))
def is_highland(code): return bool(_autoresolve(code) & (2**4))
def is_highlands(code): return is_highlands(code)
def is_swamp(code): return bool(_autoresolve(code) & (2**5))
def is_waste(code): return bool(_autoresolve(code) & (2**6))
def is_forest(code): return bool(_autoresolve(code) & (2**7))
def is_farm(code): return bool(_autoresolve(code) & (2**8))
def is_nostart(code): return bool(_autoresolve(code) & (2**9))
def is_magic(code): return bool(_autoresolve(code) & (2**10))
def is_deep(code): return bool(_autoresolve(code) & (2**11))
def is_deepsea(code): return bool(_autoresolve(code) & ((2**11) | (2**2)))
def is_cave(code): return bool(_autoresolve(code) & (2**12))

def is_fire_site(code): return bool(_autoresolve(code) & (2**13))
def is_air_site(code): return bool(_autoresolve(code) & (2**14))
def is_water_site(code): return bool(_autoresolve(code) & (2**15))
def is_earth_site(code): return bool(_autoresolve(code) & (2**16))
def is_astral_site(code): return bool(_autoresolve(code) & (2**17))
def is_death_site(code): return bool(_autoresolve(code) & (2**18))
def is_nature_site(code): return bool(_autoresolve(code) & (2**19))
def is_blood_site(code): return bool(_autoresolve(code) & (2**20))
def is_holy_site(code): return bool(_autoresolve(code) & (2**21))

def is_mountain(code): return bool(_autoresolve(code) & (2**22))
def is_mountains(code): return is_mountain(code)
def is_throne(code): return bool(_autoresolve(code) & (2**24))
def is_start(code): return bool(_autoresolve(code) & (2**25))
def is_nothrone(code): return bool(_autoresolve(code) & (2**26))

def is_warmer(code): return bool(_autoresolve(code) & (2**29))
def is_cooler(code): return bool(_autoresolve(code) & (2**30))
def is_colder(code): return is_cooler(code)

def add_terrain(base, *additions): 
    out = _autoresolve(base)
    for code in additions: out |= _autoresolve(code)
    return out

def remove_terrain(base, *additions):
    out = _autoresolve(base)
    for code in additions: out &= ~(out & _autoresolve(code))
    return out

def contains_terrain(code, *additions):
    if all([bool(code & _autoresolve(addition)) for addition in additions]):
        return True
    return False

def to_strings(code):
    code = _autoresolve(code)
    return [_terrain[i] for i in _terrain if (code & (2**i))]
