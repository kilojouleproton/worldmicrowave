#!/usr/bin/env python
import unittest
import numpy as np
from worldmicrowave import gridder
#import matplotlib.pyplot as plt

class TestPlacers(unittest.TestCase): 
    def test_poisson_disk(self):
        grid1 = gridder.PoissonDiskPlacer2D((8, 8), 3, 3)
        grid1 = gridder.PoissonDiskPlacer2D((8, 8), 4, 4)

class TestShaper(unittest.TestCase):
    def test_2d_shaper(self):
        for mapshape in (480,640), (600,800), (768,1024), (900,1440), (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(4, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1

    def test_2d_shape_picker(self):
        for mapshape in (480,640), (600,800), (768,1024), (900,1440), (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(4, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1
                        highgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=1)
                        assert np.product(highgridshape) >= nprovinces
                        lowgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=-1)
                        assert (nprovinces*2) >= np.product(lowgridshape) >= (nprovinces/2)

    def test_2d_grid_generator(self):
        for mapshape in (480,640), (600,800), (768,1024), (900,1440), (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(4, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1
                        highgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=1)
                        assert np.product(highgridshape) >= nprovinces

                        grid = gridder.RectGrid(highgridshape)
                        assert len(grid.get_neighbors((0, 0))) == 6
                        assert len(grid.get_neighbors((-1, -1))) == 6
                        grid.shuffle_diagonals()
                        #print(grid.to_ascii())

    @unittest.skip("Way too slow")
    def test_2d_poisson_disk_placer(self):
        eq = 0
        gt = 0
        lt = 0
        for mapshape in (480,640), (600,800), (768,1024), (900,1440), (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(12, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1
                        highgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=1)
                        assert np.product(highgridshape) >= nprovinces

                        grid = gridder.RectGrid(highgridshape)
                        assert len(grid.get_neighbors((0, 0))) == 6
                        assert len(grid.get_neighbors((-1, -1))) == 6
                        grid.shuffle_diagonals()


                        for nthrones in (nplayers//2, nplayers):
                            placer = gridder.PoissonDiskPlacer2D(grid.shape, nplayers, nthrones)
                            testgrid = grid.copy()
                            points = []
                            for _ in range(30):
                                points = placer.place()
                                if len(points) == (nplayers + nthrones): break
                            #print(nplayers+nthrones, np.product(grid.shape)/(nplayers+nthrones), len(points))
                            #for r,c in points:
                            #    testgrid.contents[r,c] = 41
                            #print(testgrid.to_ansi())
                            #input()
                            #if len(points) == (nplayers + nthrones):
                            #    eq += 1
                            #elif len(points) > (nplayers + nthrones): 
                            #    gt += 1
                            #else: 
                            #    lt += 1
                            #print("=", eq, ">", gt, "<", lt)

    @unittest.skip
    def test_2d_forcefield_placer(self):
        for mapshape in (480,640), (600,800), (768,1024), (900,1440), (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(12, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1
                        highgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=1)
                        assert np.product(highgridshape) >= nprovinces

                        grid = gridder.RectGrid(highgridshape)
                        assert len(grid.get_neighbors((0, 0))) == 6
                        assert len(grid.get_neighbors((-1, -1))) == 6
                        grid.shuffle_diagonals()


                        for nthrones in (nplayers//2, nplayers):
                            placer = gridder.ForceFieldPlacer2D(grid.shape, nplayers, nthrones)
                            testgrid = grid.copy()
                            points = placer.place()
                            print(points)
                            for r, c in points:
                                grid.contents[r,c] = 41
                            print(grid.to_ansi())
                            input()

    @unittest.skip("30s, just enable when needed")
    def test_2d_yeetray_placer(self):
        for mapshape in (1000,2000), (1080,1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(8, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                        #1px tolerance should be fine
                        assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1
                        highgridshape = gridder.pick_gridshape(nprovinces, gridshape, prefer=1)
                        assert np.product(highgridshape) >= nprovinces

                        grid = gridder.RectGrid(highgridshape)
                        assert len(grid.get_neighbors((0, 0))) == 6
                        assert len(grid.get_neighbors((-1, -1))) == 6
                        grid.shuffle_diagonals()


                        for nthrones in (int(np.ceil(nplayers/2)), nplayers):
                            placer = gridder.QuadraticYeetRayPlacer2D(grid.shape, nplayers, nthrones)
                            testgrid = grid.copy()
                            starts, thrones = placer.place()
                            if len(starts): testgrid.contents[starts[:,0],starts[:,1]] = 41
                            if len(thrones): testgrid.contents[thrones[:,0],thrones[:,1]] = 42

                            print(testgrid.to_ansi())
                            for start in starts:
                                for throne in thrones:
                                    assert not (start == throne).all()

    def test_is_neighbor(self):
        grid = gridder.RectGrid((4,4))
        assert not grid.is_neighbor([0,0], [0,0])
        assert grid.is_neighbor([0,0], [0,1])
        assert grid.is_neighbor([0,0], [1,1])
        assert grid.is_neighbor([0,0], [1,0])
        assert not grid.is_neighbor([0,1], [1,0])
        assert grid.is_neighbor([0,1], [1,1])
        assert not grid.is_neighbor([0,1], [0,1])
        assert grid.is_neighbor([0,1], [0,0])
        assert grid.is_neighbor([1,1], [0,0])
        assert grid.is_neighbor([1,1], [0,1])
        assert not grid.is_neighbor([1,1], [1,1])
        assert grid.is_neighbor([1,1], [1,0])
        assert not grid.is_neighbor([1,0], [1,0])
        assert grid.is_neighbor([1,0], [1,1])
        assert not grid.is_neighbor([1,0], [0,1])
        assert grid.is_neighbor([1,0], [0,0])

        assert not grid.is_neighbor([0,0], [0,0])
        assert not grid.is_neighbor([0,0], [0,2])
        assert not grid.is_neighbor([0,0], [2,2])
        assert not grid.is_neighbor([0,0], [2,0])
        assert not grid.is_neighbor([0,2], [2,0])
        assert not grid.is_neighbor([0,2], [2,2])
        assert not grid.is_neighbor([0,2], [0,2])
        assert not grid.is_neighbor([0,2], [0,0])
        assert not grid.is_neighbor([2,2], [0,0])
        assert not grid.is_neighbor([2,2], [0,2])
        assert not grid.is_neighbor([2,2], [2,2])
        assert not grid.is_neighbor([2,2], [2,0])
        assert not grid.is_neighbor([2,0], [2,0])
        assert not grid.is_neighbor([2,0], [2,2])
        assert not grid.is_neighbor([2,0], [0,2])
        assert not grid.is_neighbor([2,0], [0,0])

        assert not grid.is_neighbor([0,0], [0,0])
        assert grid.is_neighbor([0,0], [0,3])
        assert grid.is_neighbor([0,0], [3,3])
        assert grid.is_neighbor([0,0], [3,0])
        assert not grid.is_neighbor([0,3], [3,0])
        assert grid.is_neighbor([0,3], [3,3])
        assert not grid.is_neighbor([0,3], [0,3])
        assert grid.is_neighbor([0,3], [0,0])
        assert grid.is_neighbor([3,3], [0,0])
        assert grid.is_neighbor([3,3], [0,3])
        assert not grid.is_neighbor([3,3], [3,3])
        assert grid.is_neighbor([3,3], [3,0])
        assert not grid.is_neighbor([3,0], [3,0])
        assert grid.is_neighbor([3,0], [3,3])
        assert not grid.is_neighbor([3,0], [0,3])
        assert grid.is_neighbor([3,0], [0,0])

        grid.diagonals *= -1

        assert not grid.is_neighbor([0,0], [0,0])
        assert grid.is_neighbor([0,0], [0,1])
        assert not grid.is_neighbor([0,0], [1,1])
        assert grid.is_neighbor([0,0], [1,0])
        assert grid.is_neighbor([0,1], [1,0])
        assert grid.is_neighbor([0,1], [1,1])
        assert not grid.is_neighbor([0,1], [0,1])
        assert grid.is_neighbor([0,1], [0,0])
        assert not grid.is_neighbor([1,1], [0,0])
        assert grid.is_neighbor([1,1], [0,1])
        assert not grid.is_neighbor([1,1], [1,1])
        assert grid.is_neighbor([1,1], [1,0])
        assert not grid.is_neighbor([1,0], [1,0])
        assert grid.is_neighbor([1,0], [1,1])
        assert grid.is_neighbor([1,0], [0,1])
        assert grid.is_neighbor([1,0], [0,0])

        assert not grid.is_neighbor([0,0], [0,0])
        assert not grid.is_neighbor([0,0], [0,2])
        assert not grid.is_neighbor([0,0], [2,2])
        assert not grid.is_neighbor([0,0], [2,0])
        assert not grid.is_neighbor([0,2], [2,0])
        assert not grid.is_neighbor([0,2], [2,2])
        assert not grid.is_neighbor([0,2], [0,2])
        assert not grid.is_neighbor([0,2], [0,0])
        assert not grid.is_neighbor([2,2], [0,0])
        assert not grid.is_neighbor([2,2], [0,2])
        assert not grid.is_neighbor([2,2], [2,2])
        assert not grid.is_neighbor([2,2], [2,0])
        assert not grid.is_neighbor([2,0], [2,0])
        assert not grid.is_neighbor([2,0], [2,2])
        assert not grid.is_neighbor([2,0], [0,2])
        assert not grid.is_neighbor([2,0], [0,0])

        assert not grid.is_neighbor([0,0], [0,0])
        assert grid.is_neighbor([0,0], [0,3])
        assert not grid.is_neighbor([0,0], [3,3])
        assert grid.is_neighbor([0,0], [3,0])
        assert grid.is_neighbor([0,3], [3,0])
        assert grid.is_neighbor([0,3], [3,3])
        assert not grid.is_neighbor([0,3], [0,3])
        assert grid.is_neighbor([0,3], [0,0])
        assert not grid.is_neighbor([3,3], [0,0])
        assert grid.is_neighbor([3,3], [0,3])
        assert not grid.is_neighbor([3,3], [3,3])
        assert grid.is_neighbor([3,3], [3,0])
        assert not grid.is_neighbor([3,0], [3,0])
        assert grid.is_neighbor([3,0], [3,3])
        assert grid.is_neighbor([3,0], [0,3])
        assert grid.is_neighbor([3,0], [0,0])

    def test_wrapdisp(self):
        disp1 = gridder.wrapdisp((10,20), (1,2), (9,6))
        assert disp1[0] == -2
        assert disp1[1] == 4

        disp2 = gridder.wrapdisp((10,20), (9,6), (1,2))
        assert disp2[0] == 2
        assert disp2[1] == -4

    def test_semicopy(self):
        grid1 = gridder.RectGrid((16,32))
        grid2 = grid1.semicopy()
        grid3 = grid1.copy()
        assert (grid1.diagonals == grid2.diagonals).all()
        grid1.shuffle_diagonals()
        assert (grid1.diagonals == grid2.diagonals).all()

        #in theory, this has a 1 in 2^512 chance of failing as a result of regenerating the original diagonals
        assert not (grid1.diagonals == grid3.diagonals).all()

    def test_flip_diagonal(self):
        grid = gridder.RectGrid((4,4))
        assert grid.is_neighbor((0,0), (1,1))
        assert not grid.is_neighbor((1,0), (0,1))

        grid.flip_diagonal((0,0), (1,1))

        assert not grid.is_neighbor((0,0), (1,1))
        assert grid.is_neighbor((1,0), (0,1))

        grid.flip_diagonal((1,0), (0,1))

        assert grid.is_neighbor((0,0), (1,1))
        assert not grid.is_neighbor((1,0), (0,1))

        assert grid.is_neighbor((3,3), (0,0))
        assert not grid.is_neighbor((3,0), (0,3))

        grid.flip_diagonal((0,0), (3,3))

        assert not grid.is_neighbor((3,3), (0,0))
        assert grid.is_neighbor((3,0), (0,3))

        grid.flip_diagonal((0,3), (3,0))

        assert grid.is_neighbor((3,3), (0,0))
        assert not grid.is_neighbor((3,0), (0,3))

    def test_get_points_by_connection_vertices(self):
        grid = gridder.RectGrid((4,4))
        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("h", (1,1))
        assert (p1 == [0,1]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [1,2]).all()
        assert (p4 == [2,2]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("v", (1,1))
        assert (p1 == [1,0]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [2,1]).all()
        assert (p4 == [2,2]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("d", (1,1))
        assert (p1 == [1,2]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [2,2]).all()
        assert (p4 == [2,1]).all()

        grid.diagonals *= -1

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("h", (1,1))
        assert (p1 == [0,2]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [1,2]).all()
        assert (p4 == [2,1]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("v", (1,1))
        assert (p1 == [2,0]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [2,1]).all()
        assert (p4 == [1,2]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("d", (1,1))
        assert (p1 == [1,1]).all()
        assert (p2 == [1,2]).all()
        assert (p3 == [2,1]).all()
        assert (p4 == [2,2]).all()

        grid.diagonals[1,1] *= -1

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("h", (1,1))
        assert (p1 == [0,2]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [1,2]).all()
        assert (p4 == [2,2]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("v", (1,1))
        assert (p1 == [2,0]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [2,1]).all()
        assert (p4 == [2,2]).all()

        p1, p2, p3, p4 = grid.get_points_by_connection_vertices("d", (1,1))
        assert (p1 == [1,2]).all()
        assert (p2 == [1,1]).all()
        assert (p3 == [2,2]).all()
        assert (p4 == [2,1]).all()

    def test_get_connection_by_points(self):
        grid = gridder.RectGrid((4,4))

        with self.assertRaises(ValueError):
            conntype, point = grid.get_connection_by_points((1,1), (3,3))
        with self.assertRaises(ValueError):
            conntype, point = grid.get_connection_by_points((1,1), (3,1))

        conntype, point = grid.get_connection_by_points((1,1), (1,2))
        assert conntype == "h"
        assert (point == [1,1]).all()

        conntype, point = grid.get_connection_by_points((1,1), (2,1))
        assert conntype == "v"
        assert (point == [1,1]).all()

        conntype, point = grid.get_connection_by_points((1,1), (2,2))
        assert conntype == "d+"
        assert (point == [1,1]).all()

        with self.assertRaises(ValueError):
            conntype, point = grid.get_connection_by_points((1,2), (2,1))

        conntype, point = grid.get_connection_by_points((0,0), (0,3))
        assert conntype == "h"
        assert (point == [0,3]).all()

        conntype, point = grid.get_connection_by_points((0,0), (3,0))
        assert conntype == "v"
        assert (point == [3,0]).all()

        conntype, point = grid.get_connection_by_points((3,3), (0,0))
        assert conntype == "d+"
        assert (point == [3,3]).all()

        with self.assertRaises(ValueError):
            conntype, point = grid.get_connection_by_points((3,0), (0,3))

        grid.diagonals[1,1] *= -1

        with self.assertRaises(ValueError):
            conntype, point = grid.get_connection_by_points((2,2), (1,1))

        conntype, point = grid.get_connection_by_points((1,2), (2,1))
        assert conntype == "d-"
        assert (point == [1,1]).all()

                        
    def test_3d_shaper(self):
        for mapshape in (480,640,2), (600,800,2), (768,1024,2), (900,1440,2), (1000,2000,2), (1080,1920,2), (480,640,3), (600,800,3), (768,1024,3), (900,1440,3), (1000,2000,3), (1080,1920,3):
            for nplayers in range(2, 16):
                for playerprovinces in range(4, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect1 in (1, 1.414, 1.618):
                        for aspect2 in (1, 1.414, 1.618):
                            aspect = (aspect1, aspect2)
                            provshape, gridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=3)
                            #1px tolerance should be fine
                            assert np.abs(np.product(provshape) * np.product(gridshape) - np.product(mapshape)) < 1


if __name__ == "__main__":
    unittest.main()
