#!/usr/bin/env python

import unittest
from worldmicrowave import terrain

class TestSimpleTerrain(unittest.TestCase):
    def test_land(self):
        assert terrain.is_sea("sea") is True
        assert terrain.is_land("smallprov") is True
        for n in range(3, 20):
            assert terrain.is_sea(2**n + 4) is True
            assert terrain.is_water(2**n + 4) is True

    def test_plains(self):
        assert terrain._autoresolve("plains") == 0
        assert terrain.is_plains(0)

class TestCompositeTerrain(unittest.TestCase):
    def test_addition(self):
        assert terrain.is_sea(terrain.add_terrain("sea", "forest"))
        x = terrain.add_terrain("sea", "forest")
        y = terrain.add_terrain("sea", "forest")
        z = terrain.add_terrain(x, y)
        assert x == y == z == terrain.add_terrain(x, x)

    def test_subtraction(self):
        assert terrain.is_land(terrain.remove_terrain(terrain.add_terrain("sea", "forest"), "sea"))
        #print(terrain.add_terrain("sea", "forest"))
        #print(terrain.remove_terrain(terrain.add_terrain("sea", "forest"), "sea"))
        #print(terrain.remove_terrain(terrain.remove_terrain(terrain.add_terrain("sea", "forest"), "sea"), "sea"))

if __name__ == "__main__":
    unittest.main()
