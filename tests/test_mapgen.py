#!/usr/bin/env python

import unittest
from worldmicrowave import *
import numpy as np


class TestRectMapgen(unittest.TestCase):
    def test_basic_maps(self):

        nationcollection = nukeparser.NationCollection()
        nationcollection.parse("mapnuke/Assets/Nations/Default.xml")
        nationcollection.parse("mapnuke/Assets/Nations/Generic.xml")
        natindices = np.arange(len(nationcollection))

        for mapshape in (1000, 2000), (1080, 1920):
            for nplayers in range(2, 16):
                for playerprovinces in range(8, 32, 4):
                    nprovinces = nplayers * playerprovinces
                    for aspect in (1, 1.414, 1.618):
                        for nthrones in (int(np.ceil(nplayers/2)), nplayers):
                            provshape, rawgridshape = gridder.grid_shaper(nprovinces, mapshape, aspect=aspect, ndims=2)
                            gridshape = gridder.pick_gridshape(nprovinces, rawgridshape, prefer=1)

                            np.random.shuffle(natindices)
                            nations = [nationcollection[index] for index in natindices[:nplayers]]

                            newmap = mapgen.RectMap(nations, gridshape, nthrones=nthrones)
                            newmap.generate()

if __name__ == "__main__":
    unittest.main()
