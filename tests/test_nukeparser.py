#!/usr/bin/env python
import unittest
from worldmicrowave import nukeparser

class TestParser(unittest.TestCase):
    def test_parsing(self):
        nc = nukeparser.NationCollection()
        nc.parse("mapnuke/Assets/Nations/Default.xml")

if __name__ == "__main__":
    unittest.main()
